# RetroPieServerSetup
Setup Retropie on top of Ubuntu Server

Get Ubuntu server from the following link http://releases.ubuntu.com/18.04/ubuntu-18.04-live-server-amd64.iso?_ga=2.174356002.129495452.1524931675-1190604424.1524931675

This should be a minimal install environment that runs on the least amount of hardware resources as possible.  This script will then setup the rest of the environment needed to get RetroPie installed with a minimal of packages.  Once that is complete it will then install and setup RetroPie and create the environment variables needed to autologin to the emulationstation system after boot.

To get and install run the following from the command line after the server setup is complete and you have created an account using pi as the username and whatever password you want

git clone --depth=1 https://github.com/Griffen8280/RetroPieServerSetup.git

cd RetroPieServerSetup

chmod +x install.sh

sudo ./install.sh
