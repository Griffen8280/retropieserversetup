#! /bin/bash

# Need Root priveleges to accomplish install
if [ $EUID != 0 ]; then
    sudo "$0" "$@"
    exit $?
fi

# Checking if directories exist and skipping forward depending on what is found
# this is mostly just to recover in the case the script crashes
if [ ! -d "/home/pi/RetroPie-Setup" ]; then
    # Add the libretro ppa
    add-apt-repository ppa:libretro/stable -y
    
    # Update the operating system first
    apt-get -y update && apt-get -y upgrade
    
    # Install all dependencies
    apt-get install --no-install-recommends -y twm xauth xinit xorg ssh libretro-*

    # If you want a more fully featured window manager change the above twm to lubuntu-core

    # Clone the RetroPie repo
    cd /home/pi
    git clone --depth=1 https://github.com/RetroPie/RetroPie-Setup.git
    
    if [ ! -d "/home/pi/RetroPie" ]; then
        # Run the RetroPie installer
        cd /home/pi/RetroPie-Setup
        ./retropie_setup.sh
    fi
fi

# Setup the autologin to the server
if [ ! -d "/etc/systemd/system/getty@tty1.service.d" ]; then
    mkdir /etc/systemd/system/getty@tty1.service.d
fi
cat <<EOF > /etc/systemd/system/getty@tty1.service.d/override.conf
[Service]
ExecStart=
ExecStart=-/sbin/agetty --noissue --autologin pi %I TERM
Type=idle
EOF

# Start emulation station after boot
cat <<EOF > /home/pi/.xsession
#!/bin/bash
/usr/bin/emulationstation &
exec /usr/bin/twm
EOF

# Setting x to start after autologin
cat <<EOF >> /home/pi/.bashrc
if [ `tty` == "/dev/tty1" ]; then
    startx
fi
EOF
